#ifndef CUSTOMGRAPHICSITEM_H
#define CUSTOMGRAPHICSITEM_H

#include <qgraphicsitem.h>

class CustomGraphicsItem : public QGraphicsPixmapItem
{
	public:
		CustomGraphicsItem();
		virtual void setParentItem(CustomGraphicsItem * parent);
		CustomGraphicsItem * parentItem();

		virtual void setDimensions(QPoint dimensions);
		virtual void setDimensions(int width, int heigth);
		virtual QPoint getDimensions();

        virtual void setPixmapImage(QString resourcePath);

	protected:
        QPoint dimensions;
		CustomGraphicsItem * parent;
};

#endif // CUSTOMGRAPHICSITEM_H
