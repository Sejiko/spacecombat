#include "player.h"
#include "gamefield.h"
#include <QDebug>

extern Gamefield * gamefield;

Player::Player(int health, QPointF position) : Entity(position, health) {
	this->isStatic = true;
	this->mass = 1;
	this->maxSpeed = 20;
	this->acceleration = 0.3;
    this->despawnRadius = 3000;
	this->setZValue(1);
	this->setPixmapImage(":/images/spaceship_starter.png");

	QPointF correctedPlayerPosition = position - this->pixmap().rect().center();
	Entity::setPos(correctedPlayerPosition);
}

void Player::setGun(Gun * gun) {
	this->gun = gun;
}

void Player::onTick() {
	int x = 0;
	int y = 0;
	if(gamefield->isKeyPressed(Qt::Key_W)) {
		y -= 1;
	}
	if(gamefield->isKeyPressed(Qt::Key_S)) {
		y += 1;
	}
	if(gamefield->isKeyPressed(Qt::Key_A)) {
		x -= 1;
	}
	if(gamefield->isKeyPressed(Qt::Key_D)) {
		x += 1;
	}

	QLineF mouseToPlayerLine( this->pixmap().rect().center() + this->pos(), QCursor::pos());
	this->setRotation(-mouseToPlayerLine.angle() + 90);

	if(gamefield->isKeyPressed(Qt::Key_Space)) {
		this->gun->shoot();
	}

	if(x || y) {
		this->move(QPointF(x, y));
	} else {
        this->velocity.setLength(this->velocity.length() - this->acceleration / 8);
		if(this->velocity.length() < this->acceleration){
			this->velocity = QLineF(0, 0, 0, 0);
		}

		gamefield->translate(this->velocity.p2());
	}
}

void Player::move(QPointF direction) {
	QLineF moveDirection = QLineF(QPointF(0, 0), direction);
	moveDirection.setLength(this->acceleration);
	moveDirection.setAngle(moveDirection.angle() - this->rotation());

	this->velocity = QLineF(QPointF(0, 0), this->velocity.p2() + moveDirection.p2());

	if(this->velocity.length() > this->maxSpeed){
		this->velocity.setLength(this->maxSpeed);
	}

	gamefield->translate(this->velocity.p2());
}
