#include <QApplication>
#include "enemy.h"
#include "gamefield.h"

#include <qdebug.h>

Gamefield * gamefield;

int main(int argc, char *argv[]){
	QApplication application(argc, argv);
	srand(QTime::currentTime().msec());
	gamefield = new Gamefield();
	gamefield->show();

	return application.exec();
}
