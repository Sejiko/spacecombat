#ifndef BULLET_H
#define BULLET_H

#include "entity.h"
#include <QDateTime>

class Bullet : public Entity {
	public:
		Bullet(QPointF position, int health);
		void onTick();
	private:
		QDateTime lifeStart;
		int lifeTime;
};

#endif // BULLET_H
