#include "healthbar.h"
#include <QDebug>

Healthbar::Healthbar(int width, double maxValue, double currentValue) : CustomGraphicsItem() {
	this->setDimensions(width, 10);
	this->maxHealth = maxValue;
	this->maxWidth = width;

	this->setPos(0, -15);
	this->setPixmapImage(":/images/health.png");

	this->update(currentValue);
}

void Healthbar::update(double health) {
	this->setDimensions((this->maxWidth/this->maxHealth) * health, this->getDimensions().y());
}
