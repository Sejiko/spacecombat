#ifndef GUN_H
#define GUN_H

#include "entity.h"
#include <QDateTime>
#include <QPoint>

class Gun: public CustomGraphicsItem {
	public:
        Gun(QPointF position, Entity * parent, int firerate, int projectileSpeed);
        int firerate;
		int projectileSpeed;

		bool shoot();
    private:
        Entity * parent;
		QDateTime lastShot;
};

#endif // GUN_H
