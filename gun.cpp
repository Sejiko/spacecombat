#include "gun.h"
#include "bullet.h"
#include "gamefield.h"
#include <QDebug>
extern Gamefield * gamefield;

Gun::Gun(QPointF position, Entity * parent, int firerate, int projectileSpeed){
	this->setPos(position);
	this->setParentItem(parent);
	this->parent = parent;
	this->firerate = firerate;
	this->projectileSpeed = projectileSpeed;
	this->setDimensions(10, 15);

	this->lastShot = QDateTime::currentDateTime();
	this->setPixmapImage("");
}

bool Gun::shoot() {
    if(this->lastShot.msecsTo(QDateTime::currentDateTime()) < this->firerate) {
		return false;
	}

	Bullet * bullet = new Bullet(this->scenePos() - this->pixmap().rect().center(), 1);
	bullet->setRotation(this->parent->rotation());
	bullet->velocity = QLineF(0, 0, 0, this->projectileSpeed);
	bullet->velocity.setAngle(-this->parent->rotation() + 90);
	bullet->velocity = QLineF(QPointF(0, 0), bullet->velocity.p2() + this->parent->velocity.p2()/2);
	bullet->onTick();
	gamefield->addItem(bullet);

	this->lastShot = QDateTime::currentDateTime();

	return true;
}
