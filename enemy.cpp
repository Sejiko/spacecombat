#include "enemy.h"
#include "gamefield.h"

extern Gamefield * gamefield;

Enemy::Enemy(QPointF position, int health, int points) : Entity (position, health) {
	this->points = points;
}

Enemy::~Enemy() {
    gamefield->removeEnemie(this);
}
