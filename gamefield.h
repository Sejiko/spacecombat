#include <QGraphicsView>
#include <QGraphicsScene>
#include <QKeyEvent>
#include "entity.h"
#include "enemy.h"
#include "player.h"

#ifndef GAMEFIELD_H
#define GAMEFIELD_H

class Gamefield: public QGraphicsView {
    public:
		Gamefield();
		void keyPressEvent(QKeyEvent * event);
		void keyReleaseEvent(QKeyEvent * event);
        void timerEvent(QTimerEvent * timer);
        void spawnEnemies();
		bool isKeyPressed(int key);
		void addItem(Entity * entity);
		void removeItem(Entity * entity);
		void removeEnemie(Enemy * enemy);
		void translate(QPointF amount);
		void addToScore(int amount);

    private:
        int timerId;
        int score;
		QGraphicsScene * scene;
		QMap<int, bool> keyMap;
		QVector<Entity *> entities;
        Player * player;
		QTransform * backgroundTranslation = new QTransform();
        int maxEnemies;
		QList<Enemy *> currentEnemies;
		QGraphicsTextItem * scoreDisplay;

		void collideEntities(Entity *entity);
		QPointF getRandomEnemyPosition();
		void setupGamefieldWindow();
        void drawGameOverScreen();
};

#endif // GAME_H
