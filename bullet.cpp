#include "bullet.h"

Bullet::Bullet(QPointF position, int health) : Entity(position, health) {
    this->lifeStart = QDateTime::currentDateTime();
    this->lifeTime = 5000;
    this->mass = 0.3;

    this->setDimensions(8, 20);
    this->setPixmapImage(":/images/bullet.png");
}

void Bullet::onTick() {
	this->move();
	if(this->lifeStart.msecsTo(QDateTime::currentDateTime()) > this->lifeTime) {
		delete this;
		return;
    }

	Entity::onTick();
}
