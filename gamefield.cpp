#include "gamefield.h"
#include <QApplication>
#include <QDebug>
#include <QtMath>

extern QApplication application;

Gamefield::Gamefield() {
    this->score = 0;
    this->maxEnemies = 10;
	this->setupGamefieldWindow();

	QPointF playerPosition = this->rect().center();
    this->player = new Player(5000, playerPosition);
    QPointF relativeGunPosition = QPointF(this->player->pixmap().rect().center().x()-4, 0);
    this->player->setGun(new Gun(relativeGunPosition, player, 200, 10));
	this->addItem(this->player);

    this->timerId = this->startTimer(1000/60);
	this->setBackgroundBrush(QBrush(QPixmap(":/images/background.png")));
}

void Gamefield::keyPressEvent(QKeyEvent * event) {
    keyMap[event->key()] = true;

    if(event->key() == Qt::Key_Escape) {
        application.quit();
    }
}

void Gamefield::keyReleaseEvent(QKeyEvent * event) {
	keyMap[event->key()] = false;
}

void Gamefield::timerEvent(QTimerEvent * timer) {
    if(!this->player->isAlive()) {
        this->drawGameOverScreen();
        this->killTimer(this->timerId);
		return;
	}

    this->spawnEnemies();

	for(int i = 0; i < this->entities.count(); i++){
		if(QLineF(this->entities[i]->pos(), player->pos()).length() > player->despawnRadius) {
			this->entities[i]->points = 0;
			delete this->entities[i];
		} else {
			this->collideEntities(this->entities[i]);
			this->entities[i]->onTick();
		}
	}
}

void Gamefield::spawnEnemies() {
    if(this->currentEnemies.length() >= maxEnemies) {
        return;
    }

    int randomizer = rand() % 5;
    int scorePoints = (randomizer == 0) ? 15 : randomizer + 1;
    Enemy * enemy = new Enemy(this->getRandomEnemyPosition(), 50, scorePoints);

    int randomOffset = rand() % 20 - 10;
    int baseMass = 5 + (randomOffset / 2);
    int baseDimension = 50 + randomOffset;
    int dimension = (baseDimension * randomizer);
    enemy->setDimensions(dimension, dimension);
    enemy->setHealth(dimension);
    enemy->setMass(baseMass * randomizer);

    if(randomizer == 0) {
		enemy->setPixmapImage(":/images/asteroid_1.png");
    } else if (randomizer == 1) {
		enemy->setPixmapImage(":/images/asteroid_2.png");
    } else if (randomizer == 2) {
		enemy->setPixmapImage(":/images/asteroid_3.png");
    } else if (randomizer == 3) {
		enemy->setPixmapImage(":/images/asteroid_4.png");
    } else if (randomizer == 4) {
		enemy->setPixmapImage(":/images/asteroid_5.png");
	}

	enemy->setRotation(rand() % 360);
	enemy->velocity = QLineF(0, 0, 0, rand() % 5);
	double velocetyAngle = QLineF(enemy->pos(), this->player->pos()).angle() + rand() % 80 - 40;
	enemy->velocity.setAngle(velocetyAngle);

	this->currentEnemies.append(enemy);
	this->addItem(this->currentEnemies.last());

	while(enemy->collidingItems().length() > 0){
		enemy->setPos(this->getRandomEnemyPosition());
	}
}

QPointF Gamefield::getRandomEnemyPosition() {
	double screenDiagonal = sqrt(pow(this->rect().width(), 2) + pow(this->rect().height(), 2));
    QLineF enemySpawnPoint = QLineF(0, 0, screenDiagonal / 2 + 100, 0);

	if(this->player->velocity.length() < 1){
		enemySpawnPoint.setAngle(rand() % 359);
	} else {
		enemySpawnPoint.setAngle(this->player->velocity.angle() + rand() % 90 - 45);
	}

	return this->player->pos() + enemySpawnPoint.p2();
}

void Gamefield::setupGamefieldWindow() {
    this->scene = new QGraphicsScene();

    this->setScene(this->scene);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QGraphicsView::setWindowState(Qt::WindowFullScreen);
    this->scene->setSceneRect(0, 0, this->width(), this->height());

	this->scoreDisplay = new QGraphicsTextItem();

	scoreDisplay->setPos(5, 5);
    scoreDisplay->setDefaultTextColor(Qt::green);
	QFont font = scoreDisplay->font();
	font.setPointSize(18);
	scoreDisplay->setFont(font);
	scoreDisplay->setZValue(1);
	this->scene->addItem(scoreDisplay);
	this->addToScore(0);
}

void Gamefield::drawGameOverScreen() {
    CustomGraphicsItem * gameOverItem = new CustomGraphicsItem();
    gameOverItem->setDimensions(this->width(),this->height());
    gameOverItem->setPixmapImage(":/images/gameoverscreen.png");
    this->scene->addItem(gameOverItem);
}

bool Gamefield::isKeyPressed(int key) {
	return keyMap[key];
}

void Gamefield::addItem(Entity * entity) {
	this->entities.append(entity);
	this->scene->addItem(entity);
}

void Gamefield::removeItem(Entity *entity) {
	int itemIndex = this->entities.indexOf(entity);
	if(itemIndex >= 0){
		this->scene->removeItem(entity);
		this->entities.remove(itemIndex);
	}
}

void Gamefield::translate(QPointF amount) {
    for(int i = 0; i < this->entities.count(); i++) {
		if(!this->entities[i]->isStatic){
			this->entities[i]->setPos(this->entities[i]->pos() - amount);
		}
	}

    QBrush brush = this->backgroundBrush();
    brush.setTransform(this->backgroundTranslation->translate(amount.x() * -0.5, amount.y() * -0.5));
	this->setBackgroundBrush(brush);
}

void Gamefield::addToScore(int amount) {
	this->score = this->score + amount;
	scoreDisplay->setPlainText("Score: " + QString::number(this->score));
}

void Gamefield::removeEnemie(Enemy * enemy) {
    for (int i = 0; i < this->currentEnemies.length(); i++) {
        if(this->currentEnemies.at(i) == enemy) {
            this->currentEnemies.removeAt(i);
            this->removeItem(enemy);
        }
    }
}

void Gamefield::collideEntities(Entity * entity) {
    for(int i = 0; i < this->entities.length(); i++) {
		if(this->entities[i] != entity && this->entities[i]->collidesWithItem(entity)) {
			entity->onCollision(this->entities[i]);
		}
	}
}
