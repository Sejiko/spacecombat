#ifndef ENEMY_H
#define ENEMY_H

#include "entity.h"

class Enemy : public Entity {
	public:
		Enemy(QPointF position, int health, int points);
		~Enemy();
};

#endif // ENEMY_H
