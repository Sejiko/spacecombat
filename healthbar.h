#ifndef HEALTHBAR_H
#define HEALTHBAR_H

#include "customgraphicsitem.h"


class Healthbar : public CustomGraphicsItem {
	public:
		Healthbar(int width, double maxHealth, double currentValue);
		void update(double health);
	private:
		double maxHealth;
		double maxWidth;
};

#endif // HEALTHBAR_H
