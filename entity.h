#ifndef ENTITY_H
#define ENTITY_H

#include "healthbar.h"
#include <QPointF>

class Entity: public CustomGraphicsItem {
	public:
        Entity(QPointF position, int health);
        virtual ~Entity();

		QLineF velocity;
        bool isStatic;
		int points;

		virtual bool damage(double amount);
		virtual bool isDamaged();
		virtual bool isAlive();
		virtual void rotate(qreal rotation);
		virtual void setRotation(qreal rotation);
        virtual void move();
        virtual void onTick();
        virtual void onCollision(Entity * entity);

		double getHealth();
		void setHealth(double amount);

		double getMass() const;
		void setMass(double amount);

	protected:
		Healthbar * healthbar = nullptr;
		double health;
        double maxHealth;
		double mass;
};

#endif // ENTITY_H
