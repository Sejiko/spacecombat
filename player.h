#ifndef PLAYER_H
#define PLAYER_H

#include "entity.h"
#include "gun.h"

class Player: public Entity, public QObject {
public:
	Gun * gun;
	int despawnRadius;
	double maxSpeed;
	double acceleration;

	Player(int health, QPointF position);
	void setGun(Gun * gun);
    void move(QPointF direction);

	void onTick();
};

#endif // PLAYER_H
