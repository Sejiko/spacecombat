#include "entity.h"
#include "gamefield.h"
#include "healthbar.h"
#include <QDebug>

extern Gamefield * gamefield;

Entity::Entity(QPointF position, int health = 1) : CustomGraphicsItem() {
    this->setPos(position);
    this->health = health;
    this->maxHealth = health;

	this->isStatic = false;
	this->points = 0;

	this->mass = 10;
    this->velocity = QLineF(0, 0, 0, 0);

	this->setDimensions(150, 150);
	this->setPixmapImage(":/images/spaceship_upgrade_3.png");
}

Entity::~Entity() {
	gamefield->addToScore(this->points);
	gamefield->removeItem(this);
}

bool Entity::damage(double amount) {
    if(!this->isAlive()) {
		return false;
	}

	this->health = this->health - amount;

    if(this->healthbar == nullptr) {
		this->healthbar = new Healthbar(this->getDimensions().x(), this->maxHealth, this->health);
		this->healthbar->setParentItem(this);
	} else {
		this->healthbar->update(this->health);
	}

	return true;
}

bool Entity::isDamaged() {
    return this->maxHealth < this->health;
}

bool Entity::isAlive() {
    return (this->health > 0) ? true : false;
}

void Entity::rotate(qreal rotation) {
	this->setRotation(this->rotation() + rotation);
}

void Entity::setRotation(qreal rotation) {
	this->setTransformOriginPoint(this->pixmap().rect().center());
	CustomGraphicsItem::setRotation(rotation);
}

void Entity::move() {
	this->setPos(this->pos() + this->velocity.p2());
}

double Entity::getHealth() {
	return this->health;
}

void Entity::setHealth(double amount) {
	this->health = amount;
	this->maxHealth = amount;
}

double Entity::getMass() const {
	return this->mass;
}

void Entity::setMass(double amount) {
	this->mass = amount;
}

void Entity::onTick() {
	if(!this->isAlive()) {
		delete this;
		return;
	}

	this->move();
}

void Entity::onCollision(Entity * entity) {
    if(entity->parentItem() != nullptr) {
		return;
	}

	double relativSpeed = QLineF(QPointF(0, 0),this->velocity.p2() + entity->velocity.p2()).length();
	entity->damage(relativSpeed * this->mass);
	this->damage(relativSpeed * entity->mass);

    if(this->isAlive()) {
        //TODO make funtion bouncePlayer || or make the math a function => double getVector(this, entity)
		double angleA = this->velocity.angle();
		double angleB = entity->velocity.angle();
		double v1 = ((entity->mass - this->mass)/(entity->mass + this->mass))*entity->velocity.length() + ((2 * this->mass)/(entity->mass + this->mass))*this->velocity.length();
		double v2 = ((this->mass - entity->mass)/(this->mass + entity->mass))*this->velocity.length() + ((2 * entity->mass)/(this->mass + entity->mass))*entity->velocity.length();
        this->velocity = QLineF(0, 0, 0, v2);
        this->velocity.setAngle(angleB);
        entity->velocity = QLineF(0, 0, 0, v1);
        entity->velocity.setAngle(angleA);
	}
}
