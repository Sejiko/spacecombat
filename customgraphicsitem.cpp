#include "customgraphicsitem.h"

CustomGraphicsItem::CustomGraphicsItem() {
	this->setPos(0, 0);
    this->dimensions = QPoint(0, 0);
    this->parent = nullptr;
}

void CustomGraphicsItem::setParentItem(CustomGraphicsItem *parent) {
	this->parent = parent;
	QGraphicsPixmapItem::setParentItem(parent);
}

CustomGraphicsItem *CustomGraphicsItem::parentItem() {
	return this->parent;
}

void CustomGraphicsItem::setDimensions(QPoint dimensions) {
	this->dimensions = dimensions;
    if(!this->pixmap().isNull()) {
		this->setPixmap(this->pixmap().scaled(this->dimensions.x(), this->dimensions.y()));
	}
}

void CustomGraphicsItem::setDimensions(int width, int heigth) {
	this->setDimensions(QPoint(width, heigth));
}

QPoint CustomGraphicsItem::getDimensions() {
	return this->dimensions;
}

void CustomGraphicsItem::setPixmapImage(QString resourcePath) {
	QPixmap pixMap = QPixmap(resourcePath);
	if(!pixMap.isNull()){
		this->setPixmap(pixMap.scaled(this->dimensions.x(), this->dimensions.y()));
	}
}
